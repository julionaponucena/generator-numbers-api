import { Request,Response } from "express";
import { INumberService } from "../services/INumbersService";


// class responsible by reaceive the requests and send the responses to the routes numbers
export class NumbersController{
    constructor(private numbersService: INumberService){}
    post(request:Request,response:Response):Response{
        
        const {seed} = request.body
        const numbers = this.numbersService.generateNumbers(seed)
        return response.status(200).json(numbers)
    }
}