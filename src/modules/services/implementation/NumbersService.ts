import { AppError } from "../../../errors/AppError";
import { INumberService, INumberResponse } from "../INumbersService";


//class where contains the business rule of Numbers
export class NumbersService implements INumberService{
    //the initial value responsabli by generate other values
    private _seed:number;
    //static correct numbers sequence
    private  readonly numbers = [
        "0.3156106622882362",
        "0.5513213466665547",
        "0.3301843000590452",
        "0.04769233684601204",
        "0.4450511749319048",
        "0.8014283726497524",
        "0.6599671903586568",
        "0.39207733233979525",
        "0.9243979984480575"
      ];
      
    constructor(){
        this._seed = 0
    }

    public generateNumbers(seed:number):INumberResponse[]{
        
        this._seed = seed

        //this is the manipuling numbers index  an values variable 
        const map = new Map<number,number>()

        //this is the final variable that return to this function
        const generateNumbers:INumberResponse[]= []

        //generator numbers iterator
        for (let i=0;i<9;i++){
            const ramdomValue = this.random()
            map.set(i,ramdomValue)
            generateNumbers.push({index:i,value:ramdomValue})
        }
        this.checkRandomNumber(map)
        
        return generateNumbers
    }

    private checkRandomNumber(array: Map<number, number>): void{
        //this function validate if the sequence numbers generated are corrects. 
        array.forEach((element) => {
            if(!this.numbers.includes(`${element}`)){
                throw new AppError('incorrect numbers sequence')
            }
              
          });
    }

    private random():number{
        //this objective of this function is generate a number from seed value.
        const x = Math.sin(this._seed++) * 10000;
        return x - Math.floor(x);
    }
}