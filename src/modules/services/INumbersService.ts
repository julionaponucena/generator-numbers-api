export interface INumberResponse{
    index:number
    value:number
}


export interface INumberService{
    generateNumbers(seed:number):INumberResponse[]
}