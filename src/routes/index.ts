import { Router } from "express";
import { numbersRoutes } from "./numbersRoutes";

const routes = Router()
routes.use('/numbers',numbersRoutes)

export {routes}