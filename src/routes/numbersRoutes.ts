import { Router } from "express";
import { numbersController } from "../injections/NumberInjection";

const numbersRoutes = Router()

numbersRoutes.post('/',(req,res)=>numbersController.post(req,res))

export {numbersRoutes}