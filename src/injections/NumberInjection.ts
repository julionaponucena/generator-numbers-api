import { NumbersService } from "../modules/services/implementation/NumbersService";
import { NumbersController } from "../modules/controllers/NumbersController";

const numbersService = new NumbersService()

const numbersController = new NumbersController(numbersService)
export {numbersController}