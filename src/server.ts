import express from "express";
import { errorMidleware } from "./middlewares/errorMiddleware";
import { routes } from "./routes";

const app = express()

app.use(express.json())
app.use(routes)
app.use(errorMidleware)

app.listen(3333,()=>console.log('Api is running on port 3333'))